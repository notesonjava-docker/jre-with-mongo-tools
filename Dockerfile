#FROM adoptopenjdk/openjdk11:alpine-jdk-11.0.3_7
FROM adoptopenjdk/openjdk11:jre-11.0.2.9-alpine


RUN apk add --no-cache mongodb-tools
RUN rm -rf /var/cache/apk
